<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Site
{

  public function isLoggedIn()
  {
    $_this =& get_instance();

    $user_session  = $_this->session->userdata;

		if ($_this->uri->segment(1) == 'admin'){
			if ((isset($user_session['logged_in'])) && ($user_session['logged_in'] == TRUE) && ($user_session['user_role'] == 'admin') ){
        //redirect(site_url('admin/dashboard'));
			}
			else{
				if (!isset($user_session['logged_in']) || ($user_session['user_role'] == 'admin') ) {
					$_this->session->sess_destroy();
					redirect(site_url('login'));
				}
        else {
          redirect(site_url('access'));
        }
			}
		}
    else {
      if ($_this->uri->segment(1) == 'management') {
        if ( (isset($user_session['logged_in'])) && ($user_session['logged_in'] == TRUE) && ($user_session['user_role'] == 'super_user') )
        {
          //redirect(site_url('management/dashboard'));
        }
        else
        {
          if (!isset($user_session['logged_in']) || (@$user_session['user_role'] == 'super_user') ) {
  					$_this->session->sess_destroy();
  					redirect(site_url('login'));
  				}
          else if (!isset($user_session['logged_in']) || (@$user_session['user_role'] == 'admin')) {
            //redirect(site_url('management/dashboard'));
          }
          else{
            redirect(site_url('access'));
          }
        }
      }
      else if ($_this->uri->segment(1) == 'user') {
        if ( (isset($user_session['logged_in'])) && ($user_session['logged_in'] == TRUE) && ($user_session['user_role'] == 'user') )
        {
          //redirect(site_url('user/dashboard'));
        }
        else
        {
          if (isset($user_session['logged_in']) || (@$user_session['user_role'] == 'user') ) {
            //$_this->session->sess_destroy();
  					//redirect(site_url('login'));
  				}
          else if (!isset($user_session['logged_in']) || (@$user_session['user_role'] == 'admin')) {
            //redirect(site_url('user/dashboard'));
          }
          else{
            redirect(site_url('access'));
          }
        }

      }
      else {
        if ( (isset($user_session['logged_in'])) && ($user_session['logged_in'] == TRUE))
        {
          if ($user_session['user_role'] == 'user') {
            redirect(site_url('user/dashboard'));
          }
          elseif ($user_session['user_role'] == 'admin') {
            redirect(site_url('admin/dashboard'));
          }
          else {
            redirect(site_url('management/dashboard'));
          }
        }
      }

    }
  }
}


 ?>
