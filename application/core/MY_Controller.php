<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class MY_Controller extends CI_Controller
{
  protected $_backend_perpage;

  function __construct()
  {
    parent::__construct();
    $this->load->model(array('User_model', 'Ruangan_model', 'User_type_model', 'Pengumuman_model', 'Peminjaman_model'));
    $this->load->helper(array('user_helper', 'template_helper'));
    $this->load->library(array('form_validation', 'session', 'Site'));

    $this->site->isLoggedIn();

  }

}


 ?>
