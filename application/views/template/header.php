<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>SIM Peminjaman</title>
    <link rel="stylesheet" href="<?= site_url()?>assets/css/tmp/bootstrap.min.css">
    <link rel="stylesheet" href="<?= site_url()?>assets/css/tmp/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="<?= site_url()?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= site_url()?>assets/css/fullcalendar.min.css">
    <link rel="stylesheet" href="<?= site_url()?>assets/css/scheduler.min.css">
    <link rel="stylesheet" href="<?= site_url()?>assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="<?= site_url()?>assets/css/tmp/style.css">
    <link href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:400,700|Lato:400,900|Quicksand|Roboto|Ubuntu" rel="stylesheet">
    <script src="<?= site_url() ?>assets/js/jscolor.js"></script>


  </head>
  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
          class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="<?=site_url();?>">Sistem Informasi Peminjaman Ruangan PENS</a>
          <div class="nav-collapse">
            <ul class="nav pull-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i>
                  <?=get_user_info('user_name');?> <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?=site_url('user#edit?id='.get_user_info('user_ID'));?>">Setting Akun</a></li>
                    <li><a href="<?=site_url('user/logout');?>">Logout</a></li>
                  </ul>
                </li>
              </ul>
            </div>
            <!--/.nav-collapse -->
          </div>
          <!-- /container -->
        </div>
        <!-- /navbar-inner -->
      </div>

    <?php
    if (get_user_info('user_role') == 'admin'){
      $url_segment = 'admin';
    }elseif (get_user_info('user_role') == 'user') {
      $url_segment = 'user';
    }else {
      $url_segment = 'management';
    }
    ?>

    <div class="subnavbar">
      <div class="subnavbar-inner">
        <div class="container">
          <ul class="mainnav">

            <li class="<?=is_active_page_print('dashboard','active');?>"><a href="<?=site_url($url_segment.'/dashboard');?>"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a> </li>
            <li class="dropdown <?=is_active_page_print('peminjaman','active');?>"><a href="<?=site_url($url_segment.'/peminjaman');?>"><i class="fa fa-calendar" aria-hidden="true"></i><span>Peminjaman</span> </a>
            </li>
            <?php if ($url_segment != 'user'): ?>
              <li class="dropdown <?=is_active_page_print('pengumuman','active');?>"><a href="<?=site_url($url_segment.'/pengumuman');?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bullhorn"></i><span>Pengumuman</span> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?=site_url($url_segment.'/pengumuman');?>">Daftar Pengumuman</a></li>
                  <li><a href="<?=site_url($url_segment.'/pengumuman#tambah');?>">Tambah Pengumuman</a></li>
                </ul>
              </li>
            <?php endif; ?>

            <?php if ($url_segment == 'admin'): ?>
              <li class="dropdown <?=is_active_page_print('ruangan','active');?>"><a href="<?=site_url('ruangan/index');?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-th-large"></i><span>Ruangan</span>  <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?=site_url($url_segment.'/ruangan');?>">Daftar Ruangan</a></li>
                  <li><a href="<?=site_url($url_segment.'/ruangan#tambah');?>">Tambah Ruangan</a></li>
                </ul>
              </li>
            <?php endif; ?>

            <?php if ($url_segment == 'admin'): ?>
              <li class="dropdown <?=is_active_page_print('user','active');?>"><a href="<?=site_url('user');?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><span>User</span> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?=site_url($url_segment.'/user');?>">Daftar Semua User</a></li>
                  <li><a href="<?=site_url($url_segment.'/user#tambah');?>">Tambah User</a></li>
                  <li><a href="<?=site_url($url_segment.'/user/type');?>">Tipe User</a></li>
                </ul>
              </li>
            <?php endif; ?>

          </ul>
        </div>
        <!-- /container -->
      </div>
      <!-- /subnavbar-inner -->
    </div>
    <!-- /subnavbar -->
