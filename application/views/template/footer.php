    <script src="<?= site_url() ?>assets/js/jquery-1.7.2.min.js"></script>
    <script src="<?= site_url() ?>assets/js/jquery-ui.min.js"></script>
    <!-- <script src="<?= site_url() ?>assets/js/popper.min.js"></script> -->
    <script src="<?= site_url() ?>assets/js/moment-with-locales.min.js"></script>
    <script src="<?= site_url() ?>assets/js/bootstrap.js"></script>
    <script src="<?= site_url() ?>assets/js/jquery.ba-bbq.min.js"></script>
    <script src="<?= site_url() ?>assets/js/fullcalendar.min.js"></script>
    <script src="<?= site_url() ?>assets/js/scheduler.min.js"></script>
    <script src="<?= site_url() ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?= site_url() ?>assets/js/site.js"></script>

  </body>
</html>
