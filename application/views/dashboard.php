<?php $this->load->view('template/header') ?>

  <?php if ($user['user_type_name'] == 'Management'): ?>
    <?php $user_type = 'management' ?>
  <?php else: ?>
    <?php $user_type = 'user' ?>
  <?php endif; ?>

  <div class="main">
    <div class="main-inner">
      <div class="container">

        <div class="row">
          <div class="span8">
            <div class="widget">
              <div class="widget-header">
                <div class="span2">
                  <img width="150px" src="../assets/img/laptop.png" alt="image">
                </div>
                <div class="span5">
                  <p class="quicksand" style="font-size:28px; padding:30px;">Selamat Datang <strong class="ubuntu"><?= $user['user_name'] ?></strong></p>
                  <br>
                </div>
              </div>
              <div class="widget-content">
                <div class="span3" style="background-color:#dadada; width:calc(85%/3);">
                  <div class="widget text-center" style="padding: 15px; margin:0px;">
                    <i class="fa fa-calendar" style="font-size:70px;color:#f2cc35;"></i>
                    <br>
                    <br>
                    <a class="btn btn-large btn-utama" href="<?= site_url($user_type.'/peminjaman') ?>">Pinjam Rungan</a><br><br>
                  </div>
                </div>
                <?php if ($user['user_role'] != 'user'): ?>
                <div class="span3" style="background-color:#dadada; width:calc(85%/3);">
                  <div class="widget text-center" style="padding: 15px; margin:0px;">
                    <i class="fa fa-bullhorn" style="font-size:70px;color:#f2cc35;"></i>
                    <br>
                    <br>
                    <a class="btn btn-large btn-utama" href="<?= site_url($user_type.'/pengumuman#tambah') ?>">Buat Pengumuman</a><br><br>
                  </div>
                </div>
                <?php endif; ?>
                <div class="span3" style="background-color:#dadada; width:calc(85%/3);">
                  <div class="widget text-center" style="padding: 15px; margin:0px;">
                    <i class="fa fa-user" style="font-size:70px;color:#f2cc35;"></i>
                    <br>
                    <br>
                    <a class="btn btn-large btn-utama" href="<?= site_url($user_type.'/user') ?>">Setting User</a><br><br>

                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <div class="widget-header">
                <h3>Pengumuman</h3>
                <hr style="border: solid 1px #525252; margin:0">
              </div>
              <div class="widget-content">
                <p class="text-right"><strong class="ubuntu">21-11-2017</strong></p>
                <p>kepada : <strong class="ubuntu">Mahasiswa</strong></p>
                <p>Oleh : <strong class="class="ubuntu"">BEM</strong></p>
                <p style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <a style="float:right" href="#">Next</a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>


<?php $this->load->view('template/footer') ?>
