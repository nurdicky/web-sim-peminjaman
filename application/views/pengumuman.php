<?php $this->load->view('template/header') ?>

  <div class="container">
    <div class="widget content" id="pengumuman-manag" style="margin:0 20%;">

      <div class="widget-header">
        <a class="btn btn-utama" href="<?= site_url('management/pengumuman#tambah') ?>">
          <i class="fa fa-plus"></i> Tambah Pengumuman</a>
      </div>
      <div class="widget-content">
        <div class="controls" id="form" style="display:none">
          <form action="tambah" method="post">
            <fieldset class="form-horizontal">

              <div class="control-group">
                <label class="control-label" for="pengumuman_date">Tanggal</label>
                <div class="controls">
                  <input type="text" name="pengumuman_date" id="pengumuman_date" class="form-control input-block-level" value="" />
                </div>

                <label class="control-label" for="pengumuman_to">Kepada</label>
                <div class="controls">
                  <input type="text" name="pengumuman_to" id="pengumuman_to" class="form-control input-block-level" value="" />
                </div>

                <label class="control-label" for="pengumuman_from">Dari</label>
                <div class="controls">
                  <input type="text" name="pengumuman_from" id="pengumuman_from" class="form-control input-block-level" value="" />
                </div>

                <label class="control-label" for="pengumuman_content">Isi Pengumuman</label>
                <div class="controls">
                  <textarea name="pengumuman_content" id="pengumuman_content" rows="3" cols="80" style="width:98%"></textarea>
                  <!-- <textarea  name="pengumuman_content" id="pengumuman_content" class="form-control input-block-level" value="" /> -->
                </div>

              </div>

            </fieldset>
          </form>
        </div>

        <div class="controls" id="view">
          <p>Oleh : </p>
          <p>Kepada : </p>
          <br>
          <p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>

        <div class="controls pull-left">
          <p>10-11-2017</p>
        </div>

        <div class="controls pull-right">
          <a class="btn btn-small btn-secondary" href="pengumuman#edit">
            <i class="fa fa-pencil"></i> Edit</a>
          <a class="btn btn-small btn-secondary" href="pengumuman#hapus">
            <i class="fa fa-trash"></i> Hapus</a>
        </div>
        <br><br>
        <hr style="border: solid 1px gray">

    </div>
  </div>

<?php $this->load->view('template/footer') ?>

<script>
		(function($){
			$(window).on("load",function(){

				$("#pengumuman-manag").mCustomScrollbar({
					setHeight:440,
					theme:"minimal-dark"
				});

			});
		})(jQuery);
	</script>
