<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Register Mamber</title>
    <link rel="stylesheet" href="<?= site_url()?>assets/css/bootstrap.min.css">
  </head>
  <body>

    <br><br>
    <div class="container">
      <h1>Form Register Member</h1>

      <form action="<?= site_url('user/register_create') ?>" method="post">

        <div class="form-group row">
          <label for="username" class="col-sm-2 col-form-label">Username</label><?= form_error('username') ?>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="username" name="username" placeholder="Username">
          </div>
        </div>
        <div class="form-group row">
          <label for="password" class="col-sm-2 col-form-label">Password</label><?= form_error('password') ?>
          <div class="col-sm-10">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          </div>
        </div>
        <div class="form-group row">
          <label for="email" class="col-sm-2 col-form-label">Email Address</label><?= form_error('email') ?>
          <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email-Address">
          </div>
        </div>
        <div class="form-group row">
          <label for="phone" class="col-sm-2 col-form-label">Phone Numbers</label><?= form_error('phone') ?>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Numbers">
          </div>
        </div>
        <div class="form-group row">
          <label for="organization" class="col-sm-2 col-form-label">Organization</label><?= form_error('organization') ?>
          <div class="col-sm-10">
            <select class="form-control" id="organization" onchange="loadOrg()">
              <?php
                  echo "<option>- Pilih Organization -</option>";
                  for ($i=0; $i < sizeof($user); $i++) {
                      echo "<option ";
                      if( ($user[$i]->user_type_category) == ($checked['checked']) ) {
                          echo "selected";
                      };
                      echo " value='".$user[$i]->user_type_category."'>".$user[$i]->user_type_category."</option>";
                  }
              ?>
            </select>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Sign in</button>
          </div>
        </div>

      </form>

    </div>

    <script type="text/javascript">

    function loadOrg()
    {
      var org = $("#organization").val();
      var url = "user/type/"+ org;
      $.ajax({
          dataType : 'json',
          type:"POST",
          url: url,
          success: function(html)
          {
            if(data.status == 'success'){
              $('select.form-control').prepend(
                    '<div class="control-group"><div class="alert alert-info">'+
                    '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                    '<strong>Berhasil!</strong> Halaman Telah Diperbaharui ...</div></div>'
                );
            }
              //$("#org_name").html(html);
          }
      });
    }

    </script>


  </body>
</html>
