<?php $this->load->view('template/header'); ?>

<div class="main">
  <div class="main-inner">
    <div class="container">

      <div class="row">
        <div class="span12">

          <div class="widget">
            <div class="widget-header">
              <a class="btn btn-large btn-utama" href="<?=site_url('admin/ruangan#tambah');?>">
                <i class="fa fa-plus"></i> Tambah Ruangan
              </a>

            </div>

            <div class="widget-content">
              <div class="controls pull-right">
                <div class="btn-group">
                  <input type="text" class="form-control" autocomplete="off" id="search" name="search" placeholder="Cari Ruangan ... ">
                </div>
              </div>

              <div class="controls pull-left">
                  <a class="btn btn-default" id="btn-check-all"><i class="fa fa-check-square"></i></a>
              </div>

              <div class="controls pull-left">
                <div class="btn-group">
                  <a class="btn btn-default" href="#">Aksi</a>
                  <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                  <ul class="dropdown-menu" id="btn-action-admin/ruangan">
                    <li><a href="<?=site_url('admin/ruangan#mass?action=hapus');?>" ><i class="fa fa-trash-o"></i> Hapus</a></li>
                  </ul>
                </div>
              </div>  <!-- /controls -->

              <div class="controls pull-right">
                <div class="btn-group">
                  <a class="btn btn-default" id="lbl-filter-ruangan">Filter</a>
                  <a class="btn btn-default dropdown-toggle white-back" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                  <ul class="dropdown-menu" id="btn-filter-ruangan">
                    <li><a href="#">kategori...</a></li>
                  </ul>
                </div>
              </div>

              <table id="tbl-ruangan" class="table table-striped">
                  <thead>
                  </thead>
                  <tbody>
                  </tbody>
              </table>

              <div class="controls pull-right">
                  <ul id="pagination-ruangan" class="pagination"></ul>
              </div>


            </div>
            <!-- /widget-content -->
          </div>


        </div>
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>
<!-- /main -->

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"> Tambah Ruangan</h3>
  </div>

  <div class="modal-body">
    <form role="form" id="form-ruangan" action="tambah">
        <fieldset class="form-horizontal">

          <div class="control-group">
            <label class="control-label" for="ruangan_name">Nama Ruangan</label>
            <div class="controls">
              <input type="text" name="ruangan_name" id="ruangan_name" class="form-control input-block-level" value="" />
            </div>

            <label class="control-label" for="ruangan_lokasi">Lokasi Ruangan</label>
            <div class="controls">
              <input type="text" name="ruangan_lokasi" id="ruangan_lokasi" class="form-control input-block-level" value="" />
            </div>

          </div>

        </fieldset>

        <input type="hidden" name="ruangan_id" id="ruangan_id" />
        <input type="hidden" name="mass_action_type" id="mass_action_type" />
    </form>
  </div>

  <div class="modal-footer">
    <button class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">Tutup</button>
    <button class="btn btn-utama" id="submit-ruangan">Tambah!</button>
  </div>
</div>

<?php $this->load->view('template/footer'); ?>
