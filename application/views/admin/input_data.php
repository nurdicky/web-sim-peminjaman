<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Input Data | Admin</title>
    <link rel="stylesheet" href="<?= site_url() ?>assets/css/bootstrap.min.css">

  </head>
  <body>

    <br><br>
    <div class="container">

      <div class="row">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ruanganModal">Input Data Ruangan</button>
      </div>
      <br>
      <div class="row">
        <button type="button" name="button" class="btn btn-default btn-large">Input Data Peminjaman</button>
      </div>

    </div>


    <div class="modal fade" id="ruanganModal" tabindex="-1" role="dialog" aria-labelledby="ruanganModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Input Data Ruangan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="recipient-name" class="form-control-label">Nama Ruangan:</label>
                <input type="text" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="message-text" class="form-control-label">Lokasi Ruangan:</label>
                <textarea class="form-control" id="message-text"></textarea>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="button" class="btn btn-primary">Tambah Data</button>
          </div>
        </div>
      </div>
    </div>

    <script src="<?= site_url() ?>assets/js/jquery-slim.min.js"></script>
    <script src="<?= site_url() ?>assets/js/popper.min.js"></script>
    <script src="<?= site_url() ?>assets/js/bootstrap.min.js"></script>

  </body>
</html>
