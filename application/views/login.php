<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Login Member</title>
    <link href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:400,700|Lato:400,900|Quicksand|Roboto|Ubuntu" rel="stylesheet">
    <link rel="stylesheet" href="<?= site_url(); ?>assets/css/bootstrap.min.css">
  </head>
  <style media="screen">

    /*
    font-family: 'Roboto', sans-serif;
    font-family: 'Lato', sans-serif;
    font-family: 'Ubuntu', sans-serif;
    font-family: 'Quicksand', sans-serif;
    font-family: 'Barlow Semi Condensed', sans-serif;
    */

    body{
      font-family: 'Ubuntu', sans-serif;
      background-image: url('assets/img/back-login.png');
      background-size: cover;
      background-position: center;
      background-repeat: no-repeat;
      background-attachment: fixed;
    }

    .wrapper{
      margin-top: 25px;
      border: solid 5px #e4d030;
      box-shadow: 0px 0px 25px 0px #e4d030;
      border-radius: 17px;
      padding: 7% 0 7% 5%;
      color: #fff;
    }

    p, h5, h6{
      font-family: 'Barlow Semi Condensed', sans-serif;
    }
    h1{
      font-family: 'Quicksand', sans-serif;
    }
    h2{
      font-family: 'Roboto', sans-serif;
    }

    #form{
      position: absolute;
      top: 130px;
      width: 100%;
      padding: 0 10%;
    }

    .img-login{
      position: absolute;
      top: -90px;
      right: 135px;
    }

    .field{
      margin-bottom: 10px;
    }

    .field input{
      width: 100%;
      padding: 10px;
      border: none;
      background: none;
      border-bottom: solid 2px #1a4671;
    }

    .login-actions{
      margin-top: 25px;
    }

    .btn-primary {
      color: #1b4679;
      background: none;
      border: solid 2px #1b4679;
    }

  </style>

  <body>

    <br><br>
    <div class="container wrapper">
      <div class="row">
        <div class="col-md-6 col-xs-12">
          <p>Hello there ...</p>
          <h3>Welcome to</h3>
          <h1>SIM Peminjaman Ruangan</h1>

          <br><br><br>

          <p>Pencarian ruangan kosong dengan cepat !!!</p>
        </div>

        <div class="col-md-6 col-xs-12">

          <img class="img-login" src="assets/img/laptop.png" alt="image">

          <form id="form" action="<?= site_url('user/login_create')?>" method="post">

            <div class="login-fields">

              <div class="field">
                <?php echo form_error('username'); ?>
                <input type="text" id="username" name="username" placeholder="Username" class="login username-field" />
              </div> <!-- /field -->

              <div class="field">
                <?php echo form_error('password'); ?>
                <input type="password" id="password" name="password" placeholder="Password" class="login password-field"/>
              </div> <!-- /password -->

            </div> <!-- /login-fields -->

            <div class="login-actions text-right">

              <button class="button btn btn-primary btn-large">Log In</button>

            </div> <!-- .actions -->

          </form>

        </div>
      </div>

    </div>

  </body>
</html>
