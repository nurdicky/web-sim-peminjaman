<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Peminjaman extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = array(
      'user' => $this->session->userdata(), );

    $this->load->view('peminjaman', $data);
  }

  public function data($param)
  {
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    {
      if ($param == 'resources') {
        $post = $this->input->post();

        if(!empty($post['peminjaman_ID'])){
          $record = $this->Peminjaman_model->get($post['peminjaman_ID']);
          echo json_encode(array('status' => 'success', 'data' => $record));
        }
        else{
          $offset = NULL;

          if(!empty($post['lokasi']) && ($post['lokasi'] != 'null')){
            $lokasi = $post['lokasi'];
            $total_rows = $this->Ruangan_model->count(array("ruangan_lokasi LIKE" => "%$lokasi%"));
            @$record = $this->Ruangan_model->get_by(
                array("ruangan_lokasi LIKE" => "%$lokasi%"),
                $this->_backend_perpage, $offset
              );
            foreach ($record as $room) {
              $row[] = array(
                'id' => $room->ruangan_ID,
                'title' => $room->ruangan_name,
                'user' => $this->User_model->get_by()
              );
            }
          }
          else {
            $record = $this->Ruangan_model->get_by_name();
            foreach ($record as $room) {
              $row[] = array(
                'id' => $room->ruangan_ID,
                'title' => $room->ruangan_name,
                'user' => $this->User_model->get_by()
              );
            }
          }

          echo json_encode($row);
        }
      }
      elseif ($param == 'events') {
        $post = $this->input->post();

        if(!empty($post['peminjaman_ID'])){
          $record = $this->Peminjaman_model->get_peminjaman(
            array('peminjaman_ID' => $post['peminjaman_ID'] ), NULL, NULL, TRUE, NULL
          );
          echo json_encode(array('status' => 'success', 'data' => $record));
        }
        else{
          $offset = NULL;

          // if(!empty($post['lokasi']) && ($post['lokasi'] != 'null')){
          // 	$lokasi = $post['lokasi'];
          // 	$total_rows = $this->Ruangan_model->count(array("ruangan_lokasi LIKE" => "%$lokasi%"));
          // 	@$record = $this->Ruangan_model->get_by(
          // 			array("ruangan_lokasi LIKE" => "%$lokasi%"),
          // 			$this->_backend_perpage, $offset
          // 		);
          //   echo json_encode();
          // }
          // else {
            $record = $this->Peminjaman_model->get_by();
            $total_rows = $this->Peminjaman_model->count();
            foreach ($record as $event) {
              $row[] = array(
                'id' => $event->peminjaman_ID,
                'resourceId' => $event->ruangan_ID,
                'start' => $event->peminjaman_start,
                'end' => $event->peminjaman_end,
                'title' => $event->peminjaman_title,
                'user' => array('user_ID' => $event->user_ID, 'data' => $this->User_model->get_by())
              );
            }
            echo json_encode($row);
          // }
        }

      }
      elseif ($param == 'lokasi') {
        $post = $this->input->post();

        if(!empty($post['peminjaman_ID'])){
          $record = $this->Peminjaman_model->get_peminjaman(
            array('peminjaman_ID' => $post['peminjaman_ID'] ), NULL, NULL, TRUE, NULL
          );
          echo json_encode(array('status' => 'success', 'data' => $record));
        }
        else{
          $offset = NULL;

          $record = $this->Ruangan_model->get_by_lokasi(NULL,NULL,NULL,FALSE,'ruangan_lokasi');
          $total_rows = $this->Ruangan_model->count();

          echo json_encode(array(
            'record' => $record,
            'total_rows' => $total_rows
          ));
        }
      }
    }
  }
}


 ?>
