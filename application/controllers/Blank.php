<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blank extends MY_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}

  public function access()
  {
    $this->load->view('access_denied');
  }
}
