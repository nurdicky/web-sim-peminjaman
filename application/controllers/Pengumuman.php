<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Pengumuman extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = array(
      'user' => $this->session->userdata(), );

    $this->load->view('pengumuman', $data);
  }
}

 ?>
