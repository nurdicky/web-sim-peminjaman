<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Ruangan extends MY_Controller
{
  protected $_backend_perpage = 5;

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = array('user' => $this->session->userdata() );

    $this->load->view('admin/ruangan', $data);
  }

  public function action($param)
  {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    {
      if ($param == 'tambah' || $param == 'update') {
        $rules = $this->Ruangan_model->rules;
				$this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == TRUE) {
          $post = $this->input->post();

          $data = array(
            'ruangan_name' => $post['ruangan_name'],
            'ruangan_lokasi' => $post['ruangan_lokasi'],
            'ruangan_identity' => $post['ruangan_identity'],
          );

          if (!empty($post['ruangan_id'])) {
            $this->Ruangan_model->update($data, array('ruangan_ID' => $post['ruangan_id'] ));
            $result = array('status' => 'success');
          }
          else {

            if ($this->Ruangan_model->insert($data)) {
              $result = array('status' => 'success');
            }
            else {
              $result = array('status' => 'failed');
            }
          }
        }
        else {
          $result = array('status' => 'failed', 'errors' => $this->form_validation->error_array());
        }

        echo json_encode($result);
      }
      elseif ($param == 'ambil') {
        $post = $this->input->post();

				if(!empty($post['ruangan_ID'])){
					$record = $this->Ruangan_model->get($post['ruangan_ID']);
					echo json_encode(array('status' => 'success', 'data' => $record));
				}
				else{
					$offset = NULL;

					if(!empty($post['hal_aktif']) && $post['hal_aktif'] > 1){
						$offset = ($post['hal_aktif'] - 1) * $this->_backend_perpage ;
					}

          if(!empty($post['lokasi']) && ($post['lokasi'] != 'null')){
						$lokasi = $post['lokasi'];
						$total_rows = $this->Ruangan_model->count(array("ruangan_lokasi LIKE" => "%$lokasi%"));
						@$record = $this->Ruangan_model->get_by(
								array("ruangan_lokasi LIKE" => "%$lokasi%"),
								$this->_backend_perpage, $offset
							);
					}

					else if(!empty($post['cari']) && ($post['cari'] != 'null')){
						$cari = $post['cari'];
						$total_rows = $this->Ruangan_model->count(array("ruangan_name LIKE" => "%$cari%"));
						@$record = $this->Ruangan_model->get_by(array("ruangan_name LIKE" => "%$cari%"),$this->_backend_perpage, $offset, FALSE, NULL);
					}

					else{
						$record = $this->Ruangan_model->get_by(NULL,$this->_backend_perpage,$offset,FALSE,NULL);
						$total_rows = $this->Ruangan_model->count();
					}

					echo json_encode(array(
						'record' => $record,
						'total_rows' => $total_rows,
						'perpage' => $this->_backend_perpage,
            'all_location' => $this->Ruangan_model->get_by_lokasi(NULL,NULL,NULL,FALSE,'ruangan_lokasi')
					) );
				}
      }
      elseif ($param == 'hapus') {
        $post = $this->input->post(NULL, TRUE);
				if(!empty($post['ruangan_id'])){
					$this->Ruangan_model->delete($post['ruangan_id']);
					$result = array('status' => 'success');
				}

				echo json_encode($result);
      }
      else if($param == 'mass'){
				$post = $this->input->post(NULL,TRUE);

				if($post['mass_action_type'] == 'hapus'){
					if(count($post['ruangan_id']) > 0){
						$ruangan_id = $post['ruangan_id'];
            foreach ($ruangan_id as $id) {
              $this->Ruangan_model->delete($id);
            }
						$result = array('status' => 'success');
						echo json_encode($result);
					}
				}

			}
    }
  }

  public function lokasi($param)
  {
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    {
      if ($param == 'ambil') {
        # code...
      }
    }
  }

}


 ?>
