<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Pengumuman extends MY_Controller
{
  protected $_backend_perpage = 5;

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = array('user' => $this->session->userdata() );

    $this->load->view('admin/pengumuman', $data);
  }

  public function action($param)
  {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    {
      if ($param == 'tambah' || $param == 'update') {
        $rules = $this->Pengumuman_model->rules;
				$this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == TRUE) {
          $post = $this->input->post();

          $data = array(
            'pengumuman_title' => $post['pengumuman_title'],
            'pengumuman_to' => $post['pengumuman_to'],
            'pengumuman_from' => $post['pengumuman_from'],
            'pengumuman_date' => $post['pengumuman_date'],
            'pengumuman_content' => $post['pengumuman_content'],
          );

          if (!empty($post['pengumuman_id'])) {
            $this->Pengumuman_model->update($data, array('pengumuman_ID' => $post['pengumuman_id'] ));
            $result = array('status' => 'success');
          }
          else {

            if ($this->Pengumuman_model->insert($data)) {
              $result = array('status' => 'success');
            }
            else {
              $result = array('status' => 'failed');
            }
          }
        }
        else {
          $result = array('status' => 'failed', 'errors' => $this->form_validation->error_array());
        }

        echo json_encode($result);
      }
      elseif ($param == 'ambil') {
        $post = $this->input->post();

				if(!empty($post['pengumuman_ID'])){
					$record = $this->Pengumuman_model->get($post['pengumuman_ID']);
					echo json_encode(array('status' => 'success', 'data' => $record));
				}
				else{
					$offset = NULL;

					if(!empty($post['hal_aktif']) && $post['hal_aktif'] > 1){
						$offset = ($post['hal_aktif'] - 1) * $this->_backend_perpage ;
					}


					if(!empty($post['cari']) && ($post['cari'] != 'null')){
						$cari = $post['cari'];
						$total_rows = $this->Pengumuman_model->count(array("pengumuman_title LIKE" => "%$cari%"));
						@$record = $this->Pengumuman_model->get_by(array("pengumuman_title LIKE" => "%$cari%"),$this->_backend_perpage, $offset, FALSE, NULL);
					}

					else{
						$record = $this->Pengumuman_model->get_by(NULL,$this->_backend_perpage,$offset,FALSE,NULL);
						$total_rows = $this->Pengumuman_model->count();
					}

					echo json_encode(array(
						'record' => $record,
						'total_rows' => $total_rows,
						'perpage' => $this->_backend_perpage,
            
					) );
				}
      }
      elseif ($param == 'hapus') {
        $post = $this->input->post(NULL, TRUE);
				if(!empty($post['pengumuman_id'])){
					$this->Pengumuman_model->delete($post['pengumuman_id']);
					$result = array('status' => 'success');
				}

				echo json_encode($result);
      }
      else if($param == 'mass'){
				$post = $this->input->post(NULL,TRUE);

				if($post['mass_action_type'] == 'hapus'){
					if(count($post['pengumuman_id']) > 0){
						$pengumuman_id = $post['pengumuman_id'];
            foreach ($pengumuman_id as $id) {
              $this->Pengumuman_model->delete($id);
            }
						$result = array('status' => 'success');
						echo json_encode($result);
					}
				}

			}
    }
  }

}
