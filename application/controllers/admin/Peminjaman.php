<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Peminjaman extends MY_Controller
{
  protected $_backend_perpage = 5;

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = array(
      'user' => $this->session->userdata(),
      'data' => array(
        'events' => $this->Peminjaman_model->get_by(),
        'users' => $this->User_model->get_user()
      )
    );

    $this->load->view('admin/peminjaman', $data);
  }

  public function action($param)
  {
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    {
      if ($param == 'tambah' || $param == 'update') {
        $rules = $this->Peminjaman_model->rules;
				$this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == TRUE) {
          $post = $this->input->post();

          $data = array(
            'peminjaman_title' => $post['peminjaman_title'],
            'peminjaman_start' => $post['peminjaman_start'],
            'peminjaman_end' => $post['peminjaman_end'],
            'user_ID' => $post['peminjaman_user'],
            'ruangan_ID' => $post['peminjaman_ruangan'],
          );

          if (!empty($post['peminjaman_id'])) {
            $this->Peminjaman_model->update($data, array('peminjaman_ID' => $post['peminjaman_id'] ));
            $result = array('status' => 'success');
          }
          else {

            if ($this->Peminjaman_model->insert($data)) {
              $result = array('status' => 'success');
            }
            else {
              $result = array('status' => 'failed');
            }
          }
        }
        else {
          $result = array('status' => 'failed', 'errors' => $this->form_validation->error_array());
        }

        echo json_encode($result);
      }
      elseif ($param == 'ambil') {
        $post = $this->input->post();

				if(!empty($post['peminjaman_ID'])){
					$record = $this->Peminjaman_model->get($post['peminjaman_ID']);
					echo json_encode(array('status' => 'success', 'data' => $record));
				}
				else{
					$offset = NULL;

          echo json_encode(array(
            'record' => $record,
            'user_all' => $this->User_model->get_by(),
            'all_location' => $this->Ruangan_model->get_by_lokasi(NULL,NULL,NULL,FALSE,'ruangan_lokasi')
          ));
        }



      }
      elseif ($param == 'hapus') {
        $post = $this->input->post(NULL, TRUE);
				if(!empty($post['peminjaman_id'])){
					$this->Peminjaman_model->delete($post['peminjaman_id']);
					$result = array('status' => 'success');
				}

				echo json_encode($result);
      }
    }
  }

  public function data($param)
  {
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    {
      if ($param == 'resources') {
        $post = $this->input->post();

				if(!empty($post['peminjaman_ID'])){
					$record = $this->Peminjaman_model->get($post['peminjaman_ID']);
					echo json_encode(array('status' => 'success', 'data' => $record));
				}
				else{
					$offset = NULL;

          if(!empty($post['lokasi']) && ($post['lokasi'] != 'null')){
						$lokasi = $post['lokasi'];
						$total_rows = $this->Ruangan_model->count(array("ruangan_lokasi LIKE" => "%$lokasi%"));
						@$record = $this->Ruangan_model->get_by(
								array("ruangan_lokasi LIKE" => "%$lokasi%"),
								$this->_backend_perpage, $offset
							);
            foreach ($record as $room) {
              $row[] = array(
                'id' => $room->ruangan_ID,
                'title' => $room->ruangan_name,
                'user' => $this->User_model->get_by()
              );
            }
					}
          else {
            $record = $this->Ruangan_model->get_by_name();
            foreach ($record as $room) {
              $row[] = array(
                'id' => $room->ruangan_ID,
                'title' => $room->ruangan_name,
                'user' => $this->User_model->get_by()
              );
            }
          }

          echo json_encode($row);
        }
      }
      elseif ($param == 'events') {
        $post = $this->input->post();

        if(!empty($post['peminjaman_ID'])){
					$record = $this->Peminjaman_model->get_peminjaman(
            array('peminjaman_ID' => $post['peminjaman_ID'] ), NULL, NULL, TRUE, NULL
          );
					echo json_encode(array('status' => 'success', 'data' => $record));
				}
				else{
          $offset = NULL;

          // if(!empty($post['lokasi']) && ($post['lokasi'] != 'null')){
					// 	$lokasi = $post['lokasi'];
					// 	$total_rows = $this->Ruangan_model->count(array("ruangan_lokasi LIKE" => "%$lokasi%"));
					// 	@$record = $this->Ruangan_model->get_by(
					// 			array("ruangan_lokasi LIKE" => "%$lokasi%"),
					// 			$this->_backend_perpage, $offset
					// 		);
          //   echo json_encode();
					// }
          // else {
            $record = $this->Peminjaman_model->get_by();
          	$total_rows = $this->Peminjaman_model->count();
            foreach ($record as $event) {
              $row[] = array(
                'id' => $event->peminjaman_ID,
                'resourceId' => $event->ruangan_ID,
                'start' => $event->peminjaman_start,
                'end' => $event->peminjaman_end,
                'title' => $event->peminjaman_title,
                'user' => array('user_ID' => $event->user_ID, 'data' => $this->User_model->get_by())
              );
            }
            echo json_encode($row);
          // }
        }

      }
      elseif ($param == 'lokasi') {
        $post = $this->input->post();

        if(!empty($post['peminjaman_ID'])){
					$record = $this->Peminjaman_model->get_peminjaman(
            array('peminjaman_ID' => $post['peminjaman_ID'] ), NULL, NULL, TRUE, NULL
          );
					echo json_encode(array('status' => 'success', 'data' => $record));
				}
				else{
          $offset = NULL;

          $record = $this->Ruangan_model->get_by_lokasi(NULL,NULL,NULL,FALSE,'ruangan_lokasi');
        	$total_rows = $this->Ruangan_model->count();

          echo json_encode(array(
            'record' => $record,
            'total_rows' => $total_rows
          ));
        }
      }
    }
  }
}
