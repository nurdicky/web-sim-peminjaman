<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class User extends MY_Controller
{
  protected $_backend_perpage = 5;

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = array('user' => $this->session->userdata() );

    $this->load->view('admin/user', $data);
  }

  public function action($param)
  {
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    {
      if($param == 'ambil'){
				$post = $this->input->post();

				if(!empty($post['user_ID'])){
					$record = $this->User_model->get($post['user_ID']);
					echo json_encode(array('status' => 'success', 'data' => $record));
				}
				else{
					$offset = NULL;

					if(!empty($post['hal_aktif']) && $post['hal_aktif'] > 1){
						$offset = ($post['hal_aktif'] - 1) * $this->_backend_perpage ;
					}

          if(!empty($post['kategori']) && ($post['kategori'] != 'null')){
						$kategori = $post['kategori'];
            $type = $this->User_type_model->get_by(array('user_type_name LIKE' => "%$kategori%"), NULL, NULL, TRUE, NULL);
            $type_id = $type->user_type_ID;
						$total_rows = $this->User_model->count(array("user_type LIKE" => "%$type_id%"));
						@$record = $this->User_model->get_user(
								array("user_type LIKE" => "%$type_id%"),
								$this->_backend_perpage, $offset
							);
					}
					else if(!empty($post['cari']) && ($post['cari'] != 'null')){
						$cari = $post['cari'];
						$total_rows = $this->User_model->count(array("user_name LIKE" => "%$cari%"));
						@$record = $this->User_model->get_user(array("user_name LIKE" => "%$cari%"),$this->_backend_perpage, $offset, FALSE, "user_ID, user_name, user_email, user_contact, user_type_name, user_role");
					}
					else{
						$record = $this->User_model->get_user(NULL,$this->_backend_perpage,$offset,FALSE, "user_ID, user_name, user_email, user_contact, user_type_name, user_role");
						$total_rows = $this->User_model->count();
					}

					echo json_encode(array(
						'record' => $record,
						'total_rows' => $total_rows,
						'perpage' => $this->_backend_perpage,
            'all_user_type' => $this->User_type_model->get_by()
					) );
				}
			}
			else if($param == 'tambah' || $param == 'update'){

				if($param == 'update'){
					$rules = $this->User_model->rules_update;
				}
				else{
					$rules = $this->User_model->rules_register;
				}

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == TRUE) {

					$post = $this->input->post();
					$data = array(
							'user_name' => $post['user_name'],
							'user_password' => bCrypt($post['user_password'],12),
							'user_email' => $post['user_email'],
							'user_contact' => $post['user_contact'],
							'user_type' => $post['user_type'],
              'user_role' => $post['user_role']
						);

					if($param == 'update'){
						unset($data['user_name']);
						unset($data['user_email']);

						if(!empty($post['user_password'])) {
							$data['user_password'] = bCrypt($post['user_password'],12);
						}
						else{
							unset($data['user_password']);
						}

						$this->User_model->update($data, array('user_ID' => $post['user_id']));
						$getID = $post['user_id'];
					}
					else{
						$getID = $this->User_model->insert($data);
					}

					$result = array('status' => 'success');
				}
				else{
					$result = array('status' => 'failed', 'errors' => $this->form_validation->error_array());
				}

				echo json_encode($result);
			}
			else if($param == 'hapus'){
				$post = $this->input->post();
				if(!empty($post['user_id'])){
					$this->User_model->delete($post['user_id']);
					$result = array('status' => 'success');
				}

				echo json_encode($result);
			}
			else if($param == 'mass'){
				$post = $this->input->post(NULL,TRUE);
				if($post['mass_action_type'] == 'hapus'){
					if(count($post['user_id']) > 0){
						foreach($post['user_id'] as $id)
						$this->User_model->delete($id);
						$result = array('status' => 'success');
						echo json_encode($result);
					}
				}

			}
    }
  }

  public function type($param='')
  {
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    {
      if ($param == 'ambil') {
        $post = $this->input->post();

				if(!empty($post['user_type_ID'])){
					$record = $this->User_type_model->get($post['user_type_ID']);
					echo json_encode(array('status' => 'success', 'data' => $record));
				}
				else{

          $offset = NULL;

					if(!empty($post['hal_aktif']) && $post['hal_aktif'] > 1){
						$offset = ($post['hal_aktif'] - 1) * $this->_backend_perpage ;
					}

          if(!empty($post['cari']) && ($post['cari'] != 'null')){
						$cari = $post['cari'];
						$total_rows = $this->User_type_model->count(array("user_type_name LIKE" => "%$cari%"));
						@$record = $this->User_type_model->get_by(array("user_type_name LIKE" => "%$cari%"),$this->_backend_perpage, $offset, FALSE);
					}
          elseif (!empty($post['user_type']) && ($post['user_type'] != 'null')){
            $record = $this->User_type_model->get_by(NULL,$this->_backend_perpage, $offset, NULL, NULL);
            $total_rows = $this->User_type_model->count();
          }
					else{
            $record = $this->User_type_model->get_by();
            $total_rows = $this->User_type_model->count();
					}

          echo json_encode(array(
						'record' => $record,
						'total_rows' => $total_rows,
            'perpage' => $this->_backend_perpage,
            'role' => $this->User_model->get_role(NULL,NULL,NULL,NULL,'user_role')
					) );

        }
      }
      elseif ($param == 'tambah' || $param == 'update') {
        $rules = $this->User_type_model->rules;
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == TRUE) {
          $post = $this->input->post();

          $data = array(
              'user_type_name' => $post['user_type_name'],
              'user_type_category' => $post['user_type_category'],
              'user_type_color' => $post['user_type_color']
            );

          if(!empty($post['user_type_id'])){
            $this->User_type_model->update($data, array('user_type_ID' => $post['user_type_id']));
          }
          else{

            $this->User_type_model->insert($data);
          }

          $result = array('status' => 'success');

          echo json_encode($result);
        }
        else{
          echo json_encode(array('status' => 'failed'));
        }


      }
      elseif ($param == 'hapus') {
        $post = $this->input->post();
				if(!empty($post['user_type_id'])){
					$this->User_type_model->delete($post['user_type_id']);
					$result = array('status' => 'success');
				}

				echo json_encode($result);
      }
      elseif ($param == 'mass') {
        $post = $this->input->post(NULL,TRUE);
				if($post['mass_action_type'] == 'hapus'){
					if(count($post['user_type_id']) > 0){
						foreach($post['user_type_id'] as $id)
						$this->User_type_model->delete($id);
						$result = array('status' => 'success');
						echo json_encode($result);
					}
				}
      }
    }
    else {
      $data = array('user' => $this->session->userdata() );
			$this->load->view('admin/user_type', $data);
    }
  }
}

 ?>
