<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Admin extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = array('user' => $this->session->userdata() );

    $this->load->view('admin/index', $data);
  }

  public function input()
  {
    $this->load->view('admin/input_data');
  }

}


 ?>
