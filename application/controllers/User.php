<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class User extends MY_Controller
{
  protected $user_detail;

  function __construct()
  {
    parent::__construct();
    $this->load->model(array('User_model'));
    $this->load->helper(array('user_helper', 'form'));
    $this->load->library(array('form_validation'));
  }

  public function login()
	{
		/*$data = array(
      'user_name'     => 'admin',
      'user_password' => bCrypt('admin', 12),
      'user_email'    => 'admin@gmail.com',
      'user_contact'  => '081513642456',
      'user_type'     => 2,
      'user_role'     => 'admin'
     );

    $view_data = $this->User_model->insert($data);

    $this->load->view('user', array('user' => $view_data, ));*/
    $this->load->view('login');
	}

  public function register()
  {
    $user_list = $this->User_model->get_user();
    $data = array('user' => $user_list, 'checked' => 0);

    $this->load->view('register', $data);
  }

  public function register_create()
  {
    $post = $this->input->post(NULL, TRUE);

    $this->form_validation->set_message('required', '%s masih kosong, tolong diisi !');
    $rules = $this->User_model->rules_register;
    $this->form_validation->set_rules($rules);

    if ($this->form_validation->run() == FALSE) {
      $this->load->view('register');
    }
    else {
      $register_data = array(
        'user_name' => $post['username'],
        'user_password' => bCrypt($post['password'], 12),
        'user_email' => $post['email'],
        'user_contact' => $post['phone'],
      );
      $this->User_model->insert($register_data);

      redirect(site_url('login'));
    }
  }

  public function login_create()
  {
    $post = $this->input->post(NULL, TRUE);

    if (isset($post['username'])) {
      $this->user_detail = $this->User_model->get_user(
        array('user_name' => $post['username']), 1, NULL, TRUE
      );
    }

    $this->form_validation->set_message('required', '%s kosong, tolong diisi !');
    $rules = $this->User_model->rules;
    $this->form_validation->set_rules($rules);

    if ($this->form_validation->run() == FALSE) {
      $this->load->view('login');
    }
    else {
      $login_data = array(
          'user_ID' => $this->user_detail->user_ID,
          'user_name' => $post['username'],
          'user_password' => $this->user_detail->user_password,
          'user_email' => $this->user_detail->user_email,
          'user_contact' => $this->user_detail->user_contact,
          'user_type' => $this->user_detail->user_type,
          'user_role' => $this->user_detail->user_role,
          'user_type_name' => $this->user_detail->user_type_name,
          'user_type_category' => $this->user_detail->user_type_category,
          'logged_in' => TRUE
        );
      $this->session->set_userdata($login_data);

      if (@get_user_info('user_role') == 'admin') {
         redirect(site_url('admin/dashboard'));
      }
      else {
        if (@get_user_info('user_role') == 'user') {
          redirect(site_url('user/dashboard'));
        }else{
          redirect(site_url('management/dashboard'));
        }
      }
    }
  }

  public function logout()
  {
    $this->session->sess_destroy();
    redirect(site_url('login'));
  }

  public function password_check($str)
  {
    $user_detail = $this->user_detail;

    if (@$user_detail->user_password == crypt($str, @$user_detail->user_password)) {
      return TRUE;
		}
		elseif (@$user_detail->user_password) {
			$this->form_validation->set_message('password_check', 'Mohon Maaf, Password anda salah !');
			return FALSE;
		}
		else{
			$this->form_validation->set_message('password_check', '%s masih kosong, Tolong diisi !');
			return FALSE;
		}
  }

  public function email_check($str)
  {
		/* bisa digunakan untuk mengecek ke dalam database nantinya */
		if ($this->User_model->count(array('user_email' => $str)) > 0){
      $this->form_validation->set_message('email_check', 'Email sudah digunakan, mohon diganti yang lain...');
      return FALSE;
    }
    else{
      return TRUE;
    }
	}

	public function username_check($str)
  {
		/* bisa digunakan untuk mengecek ke dalam database nantinya */
		if ($this->User_model->count(array('user_name' => $str)) > 0){
      $this->form_validation->set_message('username_check', 'Username sudah digunakan, mohon diganti yang lain...');
      return FALSE;
    }
    else{
      return TRUE;
    }
	}

  public function type($param)
  {
    $user = $this->User_model->get_user(array('user_type_category' => $param));

    json_encode(array('status' => 'success', 'user' => $user));
  }
}


?>
