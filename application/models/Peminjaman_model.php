<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Peminjaman_model extends MY_Model
{
  protected $_table_name = 'peminjaman';
  protected $_order_by = 'peminjaman_ID';
  protected $_order_by_type = 'DESC';
  protected $_primary_key = 'peminjaman_ID';

  public $rules = array(
      'peminjaman_title' => array(
        'field' => 'peminjaman_title',
        'label' => 'Jenis Acara',
        'rules' => 'trim|required'
      ),
      'peminjaman_user' => array(
        'field' => 'peminjaman_user',
        'label' => 'Peminjam',
        'rules' => 'trim|required'
      ),
      // 'peminjaman_start' => array(
      //   'field' => 'peminjaman_start',
      //   'label' => 'Tanggal Mulai',
      //   'rules' => 'trim|required'
      // ),
      // 'peminjaman_end' => array(
      //   'field' => 'peminjaman_end',
      //   'label' => 'Tanggal Berakhir',
      //   'rules' => 'trim|required'
      // ),
    );

  function __construct()
  {
    parent::__construct();
  }

  function get_peminjaman($where = NULL, $limit = NULL, $offset= NULL, $single=FALSE, $select=NULL)
  {
    $this->db->join('ruangan', 'ruangan.ruangan_ID  = peminjaman.ruangan_ID');
    $this->db->join('user', 'user.user_ID  = peminjaman.user_ID');
    $this->db->join('user_type', 'user_type.user_type_ID  = user.user_type');
    return parent::get_by($where,$limit,$offset,$single,$select);
  }
}

 ?>
