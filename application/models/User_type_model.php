<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class User_type_model extends MY_Model
{
  protected $_table_name = 'user_type';
  protected $_order_by = 'user_type_ID';
  protected $_order_by_type = 'DESC';
  protected $_primary_key = 'user_type_ID';

  public $rules = array(
    'user_type_name' => array(
      'field' => 'user_type_name',
      'label' => 'Nama',
      'rules' => 'trim|required'
    ),
    'user_type_category' => array(
      'field' => 'user_type_name',
      'label' => 'Kategori',
      'rules' => 'trim|required'
    )
  );

  function __construct()
  {
    parent::__construct();
  }
}

 ?>
