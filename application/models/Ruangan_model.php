<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Ruangan_model extends MY_Model
{
  protected $_table_name = 'ruangan';
  protected $_order_by = 'ruangan_ID';
  protected $_order_by_type = 'DESC';
  protected $_primary_key = 'ruangan_ID';

  public $rules = array(
			'ruangan_name' => array(
				'field' => 'ruangan_name',
				'label' => 'Nama Ruangan',
				'rules' => 'trim|required'
			),
      'ruangan_lokasi' => array(
        'field' => 'ruangan_lokasi',
        'label' => 'Lokasi Ruangan',
        'rules' => 'trim|required'
      ),
      'ruangan_identity' => array(
        'field' => 'ruangan_identity',
        'label' => 'Identitas Ruangan',
        'rules' => 'trim|required'
      ),
		);

  function __construct()
  {
    parent::__construct();
  }

  function get_by_lokasi($where = NULL, $limit = NULL, $offset= NULL, $single=FALSE, $select=NULL){
		$this->db->group_by('ruangan_lokasi');
		return parent::get_by($where,$limit,$offset,$single,$select);
	}

  function get_by_name($where = NULL, $limit = NULL, $offset= NULL, $single=FALSE, $select=NULL){
		$this->db->order_by('ruangan_name');
		return parent::get_by($where,$limit,$offset,$single,$select);
	}


}


 ?>
