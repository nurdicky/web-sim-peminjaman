<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class User_model extends MY_Model
{
  protected $_table_name = 'user';
  protected $_order_by = 'user_ID';
  protected $_order_by_type = 'DESC';
  protected $_primary_key = 'user_ID';

  public $rules = array(
		'username' => array(
      'field' => 'username',
      'label' => 'Username',
      'rules' => 'trim|required'
		),
		'password' => array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required|callback_password_check'
		)
	);

  public $rules_update = array(
    'user_name' => array(
            'field' => 'user_name',
            'label' => 'Username',
            'rules' => 'trim|required'
    ),
    'user_email' => array(
            'field' => 'user_email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email'
    ),
  );

	public $rules_register = array(
		'user_name' => array(
      'field' => 'user_name',
      'label' => 'Username',
      'rules' => 'trim|required'
		),
		'user_password' => array(
			'field' => 'user_password',
			'label' => 'Password',
			'rules' => 'trim|required|min_length[5]'
		),
		'user_email' => array(
      'field' => 'user_email',
      'label' => 'Email',
      'rules' => 'trim|required|valid_email'
		),
    'user_contact' => array(
      'field' => 'user_contact',
      'label' => 'Phone Numbers',
      'rules' => 'trim|required'
		)
	);

  function __construct()
  {
    parent::__construct();
  }

  function get_user($where = NULL, $limit = NULL, $offset= NULL, $single=FALSE, $select=NULL)
  {
    $this->db->join('user_type', 'user.user_type  = user_type.user_type_ID');
    return parent::get_by($where,$limit,$offset,$single,$select);
  }

  function get_user_type($where = NULL, $limit = NULL, $offset= NULL, $single=FALSE, $select=NULL)
  {
    $this->db->join('user_type', 'user.user_type  = user_type.user_type_ID');
    $this->db->group_by('user_type.user_type_name');
    return parent::get_by($where,$limit,$offset,$single,$select);
  }

  function get_role($where = NULL, $limit = NULL, $offset= NULL, $single=FALSE, $select=NULL)
  {
    $this->db->group_by('user_role');
    return parent::get_by($where,$limit,$offset,$single,$select);
  }
}


?>
