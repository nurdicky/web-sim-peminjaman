<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Pengumuman_model extends MY_Model
{
  protected $_table_name = 'pengumuman';
  protected $_order_by = 'pengumuman_ID';
  protected $_order_by_type = 'DESC';
  protected $_primary_key = 'pengumuman_ID';

  public $rules = array(
			'pengumuman_title' => array(
				'field' => 'pengumuman_title',
				'label' => 'Title Pengumuman',
				'rules' => 'trim|required'
			),
      'pengumuman_to' => array(
        'field' => 'pengumuman_to',
        'label' => 'Kepada',
        'rules' => 'trim|required'
      ),
      'pengumuman_from' => array(
        'field' => 'pengumuman_from',
        'label' => 'Dari',
        'rules' => 'trim|required'
      ),

      'pengumuman_date' => array(
        'field' => 'pengumuman_date',
        'label' => 'Tanggal Pengumuman',
        'rules' => 'trim|required'
      ),
      'pengumuman_content' => array(
        'field' => 'pengumuman_content',
        'label' => 'Isi Pengumuman',
        'rules' => 'trim|required'
      ),
		);

  function __construct()
  {
    parent::__construct();
  }

}

 ?>
