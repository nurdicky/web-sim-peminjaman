var host = window.location.hostname;
var path = window.location.pathname;

var initialLocaleCode = moment.locale('id');
var sekarang = moment.today;

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout(timer);
    timer = setTimeout(callback,ms);
  };
})();

$(function(){
  $(window).hashchange(function () {
    var hash = $.param.fragment();
    console.log("hash : "+ hash);

    if (hash == 'tambah') {
      if (path.search('admin/user/type') > 0 ) {
        $('#myModal .modal-header #myModalLabel').text('Tambah Tipe User');
        $('#myModal .modal-footer #submit-user-type').text('Tambah!');
        $('#myModal #form-user-type').attr('action', 'tambah');
      }
      else if (path.search('admin/ruangan') > 0 ) {
        $('#myModal .modal-header #myModalLabel').text('Tambah Ruangan');
        $('#myModal .modal-footer #submit-ruangan').text('Tambah!');
        $('#myModal #form-ruangan').attr('action', 'tambah');
      }
      else if (path.search('admin/user') > 0 ) {

        /* BAGIAN ATRIBUT TIPE & ROLE USER*/
        var tipe_user = getJSON('http://'+host+path+'/type/ambil',{});
        var htmlStr, htmlStr2 = "";

        htmlStr = htmlStr + '<option value=""> -- Pilih tipe user -- </option>';
        htmlStr2 = htmlStr2 + '<option value=""> -- Pilih role user -- </option>';

        for (var i = 0; i < tipe_user.record.length; i++) {
          htmlStr = htmlStr + '<option value="'+tipe_user.record[i].user_type_ID+'"> '+ tipe_user.record[i].user_type_name +' </option>';
          $('#form-user #user_type').html(htmlStr);
        }

        for (var i = 0; i < tipe_user.role.length; i++) {
          htmlStr2 = htmlStr2 + '<option value="'+tipe_user.role[i].user_role+'"> '+ tipe_user.role[i].user_role +' </option>';
          $('#form-user #user_role').html(htmlStr2);
        }

        /* END BAGIAN ATRIBUT */
        $('#myModal .modal-header #myModalLabel').text('Tambah User');
        $('#myModal .modal-footer #submit-user').text('Tambah!');
        $('#myModal #form-user').attr('action', 'tambah');
      }
      else if (path.search('admin/pengumuman') > 0 ) {
        // var tipe_user = getJSON('http://'+host+path+'/type/ambil',{});
        // var htmlStr, htmlStr2 = "";
        //
        // htmlStr = htmlStr + '<option value=""> -- Pilih User -- </option>';
        // for (var i = 0; i < tipe_user.user.length; i++) {
        //   htmlStr = htmlStr + '<option value="'+tipe_user.record[i].user_type_ID+'"> '+ tipe_user.record[i].user_type_name +' </option>';
        //   $('#form-user #user_type').html(htmlStr);
        // }

        $('#myModal .modal-header #myModalLabel').text('Tambah Pengumuman');
        $('#myModal .modal-footer #submit-pengumuman').text('Tambah!');
        $('#myModal #form-pengumuman').attr('action', 'tambah');
      }
      $('#myModal').modal('show');

      if (path.search('management/pengumuman') > 0) {
        $('.widget .widget-header a').remove();
        $('.widget .widget-content #view').remove();
        $('.widget .widget-content .pull-right a').remove();
        $('.widget .widget-content .pull-left p').remove();
        $('.widget .widget-content hr').remove();
        $('.widget .widget-content #form').attr('style', 'display:block');
        $('.widget .widget-header').prepend('<h3>Tambah Pengumuman</h3>');
        $('.widget .widget-content .pull-right').prepend('<a class="btn btn-small btn-utama" href=""><i class="fa fa-save"></i> Tambah</a>');
        $('.widget .widget-content .pull-left').prepend('<a class="btn btn-small btn-secondary" href="pengumuman"><i class="fa fa-arrow-left"></i> Kembali</a>');

      }
    }
    else if (hash.search('edit') == 0) {
      if (path.search('admin/user/type') > 0 ) {
        var type_ID = getUrlVars()['id'];
        var type_detail = getJSON('http://'+host+path+'/ambil', {user_type_ID:type_ID});
        console.log(type_detail);
        $('#myModal .modal-body #user_type_name').val(type_detail.data['user_type_name']);
        $('#myModal .modal-body #user_type_category').val(type_detail.data['user_type_category']);
        $('#myModal .modal-body #user_type_color').val(type_detail.data['user_type_color']);
        $('#myModal .modal-body #user_type_color').attr('style','background-color: #'+type_detail.data['user_type_color']+'');
        $('#myModal .modal-header #myModalLabel').text('Edit Tipe User');
        $('#myModal .modal-footer #submit-user-type').text('Update!');
        $('#myModal #form-user-type').attr('action', 'update');
        $('#myModal #form-user-type #user_type_id').val(type_ID);
      }
      else if (path.search('admin/ruangan') > 0 ) {
        var ruangan_ID = getUrlVars()['id'];
        var ruangan_detail = getJSON('http://'+host+path+'/action/ambil', {ruangan_ID:ruangan_ID});
        $('#myModal .modal-body #ruangan_name').val(ruangan_detail.data['ruangan_name']);
        $('#myModal .modal-body #ruangan_lokasi').val(ruangan_detail.data['ruangan_lokasi']);
        $('#myModal .modal-header #myModalLabel').text('Edit Ruangan');
        $('#myModal .modal-footer #submit-ruangan').text('Update!');
        $('#myModal #form-ruangan').attr('action', 'update');
        $('#myModal #form-ruangan #ruangan_id').val(ruangan_ID);
      }
      else if (path.search('admin/user') > 0) {
        var user_ID = getUrlVars()['id'];
        var user_detail = getJSON('http://'+host+path+'/action/ambil', {user_ID:user_ID});

        for (var i in user_detail.data) {
          if (i == 'user_password') {
            $('#form-user #'+i).val();
            $('#form-user #'+i).after('<p class="help-block warning">kosongkan jika tidak ingin merubah password</p>');
          }
        }
        $('#myModal .modal-body #user_name').val(user_detail.data['user_name']);
        $('#myModal .modal-body #user_email').val(user_detail.data['user_email']);
        $('#myModal .modal-body #user_contact').val(user_detail.data['user_contact']);
        $('#myModal .modal-body #user_type').val(user_detail.data['user_type']);
        $('#myModal .modal-body #user_role').val(user_detail.data['user_role']);
        $('#myModal .modal-header #myModalLabel').text('Edit User');
        $('#myModal .modal-footer #submit-user').text('Update!');
        $('#myModal #form-user').attr('action', 'update');
        $('#myModal #form-user #user_id').val(user_ID);
      }
      else if (path.search('admin/pengumuman') > 0 ) {
        var pengumuman_ID = getUrlVars()['id'];
        var pengumuman_detail = getJSON('http://'+host+path+'/action/ambil', {pengumuman_ID:pengumuman_ID});
        $('#myModal .modal-body #pengumuman_title').val(pengumuman_detail.data['pengumuman_title']);
        $('#myModal .modal-body #pengumuman_to').val(pengumuman_detail.data['pengumuman_to']);
        $('#myModal .modal-body #pengumuman_from').val(pengumuman_detail.data['pengumuman_from']);
        $('#myModal .modal-body #pengumuman_date').val(pengumuman_detail.data['pengumuman_date']);
        $('#myModal .modal-body #pengumuman_content').val(pengumuman_detail.data['pengumuman_content']);
        $('#myModal .modal-header #myModalLabel').text('Edit Pengumuman');
        $('#myModal .modal-footer #submit-pengumuman').text('Update!');
        $('#myModal #form-pengumuman').attr('action', 'update');
        $('#myModal #form-pengumuman #pengumuman_id').val(pengumuman_ID);
      }
      $('#myModal').modal('show');
    }
    else if (hash.search('hapus') == 0) {
      if (path.search('admin/user/type') > 0 ) {
        var type_ID = getUrlVars()["id"];
        var type_detail = getJSON('http://'+host+path+'/ambil',{user_type_ID: type_ID});

        $('#myModal form').hide();
        $('#myModal .modal-header #myModalLabel').text('Hapus Tipe User');
        $('#myModal .modal-footer #submit-user-type').text('Ya Hapus Saja!');
        $('#myModal #form-user-type').attr('action', 'hapus');
        $('#myModal .modal-body').prepend('<p id="hapus-notif">Apakah Anda yakin akan menghapus ruangan : <b>"'+type_detail.data['user_type_name']+'"</b> ???</p>');
        $('#myModal #form-user-type #user_type_id').val(type_ID);
      }
      else if (path.search('admin/ruangan') > 0 ) {
        var ruangan_ID = getUrlVars()["id"];
        var ruangan_detail = getJSON('http://'+host+path+'/action/ambil',{ruangan_ID: ruangan_ID});

        $('#myModal form').hide();
        $('#myModal .modal-header #myModalLabel').text('Hapus Ruangan');
        $('#myModal .modal-footer #submit-ruangan').text('Ya Hapus Saja!');
        $('#myModal #form-ruangan').attr('action', 'hapus');
        $('#myModal .modal-body').prepend('<p id="hapus-notif">Apakah Anda yakin akan menghapus ruangan : <b>"'+ruangan_detail.data['ruangan_name']+'"</b> ???</p>');
        $('#myModal #form-ruangan #ruangan_id').val(ruangan_ID);
      }
      else if (path.search('admin/user') > 0 ) {
        var user_ID = getUrlVars()["id"];
        var user_detail = getJSON('http://'+host+path+'/action/ambil',{user_ID: user_ID});

        $('#myModal form').hide();
        $('#myModal .modal-header #myModalLabel').text('Hapus User');
        $('#myModal .modal-footer #submit-user').text('Ya Hapus Saja!');
        $('#myModal #form-user').attr('action', 'hapus');
        $('#myModal .modal-body').prepend('<p id="hapus-notif">Apakah Anda yakin akan menghapus user : <b>"'+user_detail.data['user_name']+'"</b> ???</p>');
        $('#myModal #form-user #user_id').val(user_ID);
      }
      else if (path.search('admin/pengumuman') > 0) {
        var pengumuman_ID = getUrlVars()["id"];
        var pengumuman_detail = getJSON('http://'+host+path+'/action/ambil',{pengumuman_ID: pengumuman_ID});

        $('#myModal form').hide();
        $('#myModal .modal-header #myModalLabel').text('Hapus Pengumuman');
        $('#myModal .modal-footer #submit-pengumuman').text('Ya Hapus Saja!');
        $('#myModal #form-pengumuman').attr('action', 'hapus');
        $('#myModal .modal-body').prepend('<p id="hapus-notif">Apakah Anda yakin akan menghapus pengumuman : <b>"'+pengumuman_detail.data['pengumuman_title']+'"</b> ???</p>');
        $('#myModal #form-pengumuman #pengumuman_id').val(pengumuman_ID);
      }
      $('#myModal').modal('show');
    }
    else if (hash.search('ambil') == 0) {
      if (path.search('admin/user/type') > 0) {
        var hal_aktif, cari = null;
        var hash = getUrlVars();

        console.log(hash);

        if (hash['cari'] && hash['hal']) {
          hal_aktif = hash['hal'];
          cari = hash['cari'];
          $('#lbl-filter-user-type').text('Filter');
        }
        else if (hash['hal']) {
          hal_aktif = hash['hal'];
        }

        ambil_user_type(hal_aktif, true, cari);
        $("ul#pagination-user-type li a:contains('"+hal_aktif+"')").parents().addClass('active').siblings().removeClass('active');

      }
      else if (path.search('admin/ruangan') > 0) {
        var hal_aktif, cari, lokasi = null;
        var hash = getUrlVars();

        if (hash['lokasi'] && hash['hal']) {
          hal_aktif = hash['hal'];
          lokasi = hash['lokasi'];
          $('#lbl-filter-ruangan').text(humanize(lokasi));
          $('#search').val("");
        }
        else if (hash['cari'] && hash['hal']) {
          hal_aktif = hash['hal'];
          cari = hash['cari'];
          $('#lbl-filter-ruangan').text('Filter');
        }
        else if (hash['hal']) {
          hal_aktif = hash['hal'];
        }

        ambil_ruangan(hal_aktif, true, lokasi, cari);
        $("ul#pagination-ruangan li a:contains('"+hal_aktif+"')").parents().addClass('active').siblings().removeClass('active');
      }
      else if (path.search('admin/user') > 0) {
        var hal_aktif, cari, kategori= null;
        var hash = getUrlVars();

        if (hash['kategori'] && hash['hal']) {
          hal_aktif = hash['hal'];
          kategori = hash['kategori'];
          $('#lbl-filter-user').text(humanize(kategori));
          $('#search').val("");
        }
        else if (hash['cari'] && hash['hal']) {
          hal_aktif = hash['hal'];
          cari = hash['cari'];
          $('#lbl-filter-user').text('Filter');
        }
        else if (hash['hal']) {
          hal_aktif = hash['hal'];
        }

        ambil_user(hal_aktif, true, kategori, cari);
        $("ul#pagination-user li a:contains('"+hal_aktif+"')").parents().addClass('active').siblings().removeClass('active');
      }
      else if (path.search('admin/peminjaman') > 0) {
        var lokasi = null;
        var hash = getUrlVars();

        if (hash['lokasi']) {
          lokasi = hash['lokasi'];
          $('#lbl-filter-lokasi').text(humanize(lokasi));
        }


        //ambil_peminjaman(lokasi, true);
        // $('#calendar').fullCalendar('render');

      }
      else if (path.search('admin/pengumuman') > 0) {
        var hal_aktif, cari = null;
        var hash = getUrlVars();

        if (hash['cari'] && hash['hal']) {
          hal_aktif = hash['hal'];
          cari = hash['cari'];
        }
        else if (hash['hal']) {
          hal_aktif = hash['hal'];
        }

        ambil_pengumuman(hal_aktif, true, cari);
        $("ul#pagination-pengumuman li a:contains('"+hal_aktif+"')").parents().addClass('active').siblings().removeClass('active');
      }
    }
    else if (hash.search('mass') == 0) {
      if (path.search('admin/user/type') > 0) {
        var action = getUrlVars()['action'];
        var numberOfChecked = $('#tbl-user-type input:checkbox:checked').length;
        if (numberOfChecked > 0) {
          if (action == 'hapus') {
            var note = 'menghapus';
          }

          $('#myModal #form-user-type').attr('action', 'mass');
          $('#myModal #form-user-type #mass_action_type').val(action);
          $('#myModal .modal-header #myModalLabel').text('Aksi Tipe User Masal');
          $('#myModal .modal-footer #submit-user-type').text('Ya Langsung Saja! ').show();
          $('#myModal .modal-body').prepend('<p id="hapus-notif">Apakah Anda yakin akan '+note+' : <b>"tipe-tipe user terpilih"</b> ???</p>');
        }
        else {
          $('#myModal .modal-header #myModalLabel').text('Peringatan!!');
          $('#myModal .modal-footer #submit-user-type').hide();
          $('#myModal #form-user-type').attr('action', 'bulk');
          $('#myModal .modal-body').prepend('<p id="hapus-notif">Mohon maaf, aksi tipe user tidak bisa dilakukan karena tidak ada satupun tipe user yang di ceklis. Silahkan ceklis satu atau beberapa ...</p>');
        }
        $('#myModal form').hide();
      }
      else if (path.search('admin/ruangan') > 0) {
        var action = getUrlVars()['action'];
        var numberOfChecked = $('#tbl-ruangan input:checkbox:checked').length;
        if (numberOfChecked > 0) {
          if (action == 'hapus') {
            var note = 'menghapus';
          }

          $('#myModal #form-ruangan').attr('action', 'mass');
          $('#myModal #form-ruangan #mass_action_type').val(action);
          $('#myModal .modal-header #myModalLabel').text('Aksi Ruangan Masal');
          $('#myModal .modal-footer #submit-ruangan').text('Ya Langsung Saja! ').show();
          $('#myModal .modal-body').prepend('<p id="hapus-notif">Apakah Anda yakin akan '+note+' : <b>"ruangan-ruangan terpilih"</b> ???</p>');
        }
        else {
          $('#myModal .modal-header #myModalLabel').text('Peringatan!!');
          $('#myModal .modal-footer #submit-ruangan').hide();
          $('#myModal #form-ruangan').attr('action', 'bulk');
          $('#myModal .modal-body').prepend('<p id="hapus-notif">Mohon maaf, aksi ruangan tidak bisa dilakukan karena tidak ada satupun ruangan yang di ceklis. Silahkan ceklis satu atau beberapa ...</p>');
        }
        $('#myModal form').hide();
      }
      else if (path.search('admin/user') > 0) {
        var action = getUrlVars()['action'];
        var numberOfChecked = $('#tbl-user input:checkbox:checked').length;
        if (numberOfChecked > 0) {
          if (action == 'hapus') {
            var note = 'menghapus';
          }

          $('#myModal #form-user').attr('action', 'mass');
          $('#myModal #form-user #mass_action_type').val(action);
          $('#myModal .modal-header #myModalLabel').text('Aksi User Masal');
          $('#myModal .modal-footer #submit-user').text('Ya Langsung Saja! ').show();
          $('#myModal .modal-body').prepend('<p id="hapus-notif">Apakah Anda yakin akan '+note+' : <b>"user-user terpilih"</b> ???</p>');
        }
        else {
          $('#myModal .modal-header #myModalLabel').text('Peringatan!!');
          $('#myModal .modal-footer #submit-user').hide();
          $('#myModal #form-user').attr('action', 'bulk');
          $('#myModal .modal-body').prepend('<p id="hapus-notif">Mohon maaf, aksi user tidak bisa dilakukan karena tidak ada satupun user yang di ceklis. Silahkan ceklis satu atau beberapa ...</p>');
        }
        $('#myModal form').hide();
      }
      else if (path.search('admin/pengumuman') > 0) {
        var action = getUrlVars()['action'];
        var numberOfChecked = $('#tbl-pengumuman input:checkbox:checked').length;
        if (numberOfChecked > 0) {
          if (action == 'hapus') {
            var note = 'menghapus';
          }

          $('#myModal #form-pengumuman').attr('action', 'mass');
          $('#myModal #form-pengumuman #mass_action_type').val(action);
          $('#myModal .modal-header #myModalLabel').text('Aksi Pengumuman Masal');
          $('#myModal .modal-footer #submit-pengumuman').text('Ya Langsung Saja! ').show();
          $('#myModal .modal-body').prepend('<p id="hapus-notif">Apakah Anda yakin akan '+note+' : <b>"pengumuman-pengumuman terpilih"</b> ???</p>');
        }
        else {
          $('#myModal .modal-header #myModalLabel').text('Peringatan!!');
          $('#myModal .modal-footer #submit-user').hide();
          $('#myModal #form-pengumuman').attr('action', 'bulk');
          $('#myModal .modal-body').prepend('<p id="hapus-notif">Mohon maaf, aksi pengumuman tidak bisa dilakukan karena tidak ada satupun pengumuman yang di ceklis. Silahkan ceklis satu atau beberapa ...</p>');
        }
        $('#myModal form').hide();
      }

      $('#myModal').modal('show');
    }

    if (path.search('admin/peminjaman') > 0) {

      var lokasi = getJSON('http://'+host+path+'/data/lokasi', {});
      $('#btn-filter-lokasi li').remove();
      $('#btn-filter-lokasi').append('<li><a href="peminjaman">Semua Lokasi</a></li>');
      for(var i in lokasi.record){
        $('#btn-filter-lokasi').append('<li><a href="peminjaman#ambil?lokasi='+lokasi.record[i]['ruangan_lokasi']+'">'+lokasi.record[i]['ruangan_lokasi']+'</a></li>');
      }

      /* ************************************** */
      /*         BAGIAN FULL CALENDAR           */
      /* ************************************** */
      if(getUrlVars()["lokasi"]){
        ambil_peminjaman(getUrlVars()["lokasi"], true);
      }
      else{ ambil_peminjaman(null, false);}
    }
    else if (path.search('management/peminjaman') > 0) {
      if(getUrlVars()["lokasi"]){
        ambil_peminjaman(getUrlVars()["lokasi"], true);
      }
      else{ ambil_peminjaman(null, false);}
    }

  });

  $(window).trigger('hashchange');

  $('#myModal').on('hidden', function(){
    window.history.pushState(null, null, path);
    $('#myModal').removeClass('big-modal');
    $('#myModal #hapus-peminjaman').remove();
    $('#myModal #hapus-notif').remove();
    $('#myModal #jscolor #user_type_color').removeAttr('style');
    $('#myModal form').find("input[type=text], input[type=hidden], input[type=password], input[type=email], textarea").val("").attr('placeholder', '');
    $('#myModal form').find("input[type=checkbox],input[type=radio]").removeAttr('checked');
    $('#myModal form').find("select").prop("selected", false);
    $('#myModal form p.warning').remove();
    $('#myModal #submit-ruangan').show();
    $('#myModal form').show();
  });

  $('#btn-check-all').toggle(function(){
      $('table input:checkbox').attr('checked','checked');
    }, function(){
      $('table input:checkbox').removeAttr('checked');
    }
  );

  $(document).on('keyup','#search', function(){
    delay(function(){
      var searchkey = $('#search').val();
      window.location.hash = "#ambil?cari="+searchkey+"&hal=1";
    }, 250);
  });

  moment.locale('id');

  /* ************************************** */
  /*        BACKEND BAGIAN RUANGAN          */
  /* ************************************** */

  $(document).on('click', '#submit-ruangan', function (eve) {
    eve.preventDefault();

    var action = $('#form-ruangan').attr('action');
    var mass_action_type = $('#form-ruangan #mass_action_type').val();

    if (action == 'mass') {
      var datatosend = $('#tbl-ruangan input').serialize() + '&mass_action_type='+mass_action_type;
    }else{
      var datatosend = $('#form-ruangan').serialize();
    }
    console.log("datatosend : "+ datatosend);

    $.ajax({
      dataType : 'json',
      type : 'POST',
      url : 'http://'+host+path+'/action/'+action,
      data : datatosend,
      success : function(data) {
        if (data.status == 'success') {
          ambil_ruangan(null, false);

          $('#myModal').modal('hide');
          $('div.widget-content').prepend(
              '<div class="control-group"><div class="alert alert-info">'+
              '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
              '<strong>Berhasil!</strong> Data telah diperbaharui ... </div></div>'
            );
        }
        else {
          $.each(data.errors, function(key, value) {
            $('#'+key).attr('placeholder', value);
          });
        }
      }
    });

  });

  /* ******************************************************************** */
  /* Ambil Ruangan (READ) */
  /* ******************************************************************** */
  if(getUrlVars()["hal"]){ ambil_ruangan(getUrlVars()["hal"],false); }
  else{ ambil_ruangan(null,false);}

  /* ************************************** */
  /*        BACKEND BAGIAN USER             */
  /* ************************************** */

  $(document).on('click', '#submit-user', function (eve) {
    eve.preventDefault();

    var action = $('#form-user').attr('action');
    var mass_action_type = $('#form-user #mass_action_type').val();

    if (action == 'mass') {
      var datatosend = $('#tbl-user input').serialize() + '&mass_action_type='+mass_action_type;
    }else{
      var datatosend = $('#form-user').serialize();
    }
    console.log("datatosend : "+ datatosend);

    $.ajax({
      dataType : 'json',
      type : 'POST',
      url : 'http://'+host+path+'/action/'+action,
      data : datatosend,
      success : function(data) {
        if (data.status == 'success') {
          ambil_user(null, false);

          $('#myModal').modal('hide');
          $('div.widget-content').prepend(
              '<div class="control-group"><div class="alert alert-info">'+
              '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
              '<strong>Berhasil!</strong> Data telah diperbaharui ... </div></div>'
            );
        }
        else {
          $.each(data.errors, function(key, value) {
            $('#'+key).attr('placeholder', value);
          });
        }
      }
    });

  });

  if(getUrlVars()["hal"]){ ambil_user(getUrlVars()["hal"],false); }
  else{ ambil_user(null,false);}

  /* ************************************** */
  /*        BACKEND BAGIAN TIPE USER        */
  /* ************************************** */

  $(document).on('click', '#submit-user-type', function (eve) {
    eve.preventDefault();

    var action = $('#form-user-type').attr('action');
    var mass_action_type = $('#form-user-type #mass_action_type').val();

    if (action == 'mass') {
      var datatosend = $('#tbl-user-type input').serialize() + '&mass_action_type='+mass_action_type;
    }else{
      var datatosend = $('#form-user-type').serialize();
    }
    console.log("datatosend : "+ datatosend);

    $.ajax({
      dataType : 'json',
      type : 'POST',
      url : 'http://'+host+path+'/'+action,
      data : datatosend,
      success : function(data) {
        if (data.status == 'success') {
          ambil_user_type(null, false);

          $('#myModal').modal('hide');
          $('div.widget-content').prepend(
              '<div class="control-group"><div class="alert alert-info">'+
              '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
              '<strong>Berhasil!</strong> Data telah diperbaharui ... </div></div>'
            );
        }
        else {
          $.each(data.errors, function(key, value) {
            $('#'+key).attr('placeholder', value);
          });
        }
      }
    });

  });

  if(getUrlVars()["hal"]){ ambil_user_type(getUrlVars()["hal"],false); }
  else{ ambil_user_type(null,false);}


  /* ************************************** */
  /*        BACKEND BAGIAN PEMINJAMAN       */
  /* ************************************** */

  $(document).on('click', '#submit-peminjaman', function (eve) {
    eve.preventDefault();

    var action = $('#form-peminjaman').attr('action');
    var datatosend = $('#form-peminjaman').serialize();

    console.log("datatosend : "+ datatosend);

    $.ajax({
      dataType : 'json',
      type : 'POST',
      url : 'http://'+host+path+'/action/'+action,
      data : datatosend,
      success : function(data) {
        if (data.status == 'success') {
          $('#calendar').fullCalendar( 'refetchEvents' );

          $('.fc-ltr .fc-timeline-event').attr('id', datatosend.user_ID);

          $('#myModal').modal('hide');
        }
        else {
          $.each(data.errors, function(key, value) {
            $('#'+key).attr('placeholder', value);
          });
        }
      }
    });

  });


  $(document).on('click', '#hapus-peminjaman', function (eve) {
    eve.preventDefault();

    var datatosend = $('#form-peminjaman').serialize();

    console.log("datatosend : "+ datatosend);

    $.ajax({
      dataType : 'json',
      type : 'POST',
      url : 'http://'+host+path+'/action/hapus',
      data : datatosend,
      success : function(data) {
        if (data.status == 'success') {
          $('#calendar').fullCalendar( 'refetchEvents' );

          $('#myModal').modal('hide');
        }
        else {
          $.each(data.errors, function(key, value) {
            $('#'+key).attr('placeholder', value);
          });
        }
      }
    });

  });


  /* ************************************** */
  /*        BACKEND BAGIAN PENGUMUMAN       */
  /* ************************************** */

  $(document).on('click', '#submit-pengumuman', function (eve) {
    eve.preventDefault();

    var action = $('#form-pengumuman').attr('action');
    var mass_action_type = $('#form-pengumuman #mass_action_type').val();

    if (action == 'mass') {
      var datatosend = $('#tbl-pengumuman input').serialize() + '&mass_action_type='+mass_action_type;
    }else{
      var datatosend = $('#form-pengumuman').serialize();
    }
    console.log("datatosend : "+ datatosend);

    $.ajax({
      dataType : 'json',
      type : 'POST',
      url : 'http://'+host+path+'/action/'+action,
      data : datatosend,
      success : function(data) {
        if (data.status == 'success') {
          ambil_pengumuman(null, false);

          $('#myModal').modal('hide');
          $('div.widget-content').prepend(
              '<div class="control-group"><div class="alert alert-info">'+
              '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
              '<strong>Berhasil!</strong> Data telah diperbaharui ... </div></div>'
            );
        }
        else {
          $.each(data.errors, function(key, value) {
            $('#'+key).attr('placeholder', value);
          });
        }
      }
    });

  });

  if(getUrlVars()["hal"]){ ambil_pengumuman(getUrlVars()["hal"],false); }
  else{ ambil_pengumuman(null,false);}

  /* MENCEGAH SUBMIT MENGGUNAKAN ENTER PADA INPUT */

  $(document).on('submit','#myModal form', function(eve){
    eve.preventDefault();
  });

});

/* ************************************** */
/*              ALL FUNCTION              */
/* ************************************** */


function ambil_ruangan(hal_aktif, scrolltop, lokasi, cari) {
  var path = window.location.pathname;
  var host = window.location.hostname;
  if ($('table#tbl-ruangan').length > 0) {
    $.ajax({
      dataType : 'json',
      type : 'POST',
      url : 'http://'+host+path+'/action/ambil',
      data : {hal_aktif:hal_aktif, lokasi:lokasi, cari:cari},
      success: function(data){

        /***********************/
        /* tampilkan datanya  */
        /**********************/
        $('table#tbl-ruangan tbody tr').remove();
        $('table#tbl-ruangan thead tr').remove();

        $('table#tbl-ruangan').find('thead').append(
          '<tr>'+
          ' <th width="2%"></th>'+
          ' <th width="50%">Nama Ruangan</th>'+
          ' <th width="33%">Lokasi Ruangan</th>'+
          ' <th width="15%"></th>'+
          '</tr>'
        );

        $.each(data.record, function(index, element) {

          $('table#tbl-ruangan').find('tbody').append(
            '<tr>'+
            '  <td width="2%"><input type="checkbox" name="ruangan_id[]" value="'+element.ruangan_ID+'"></td>'+
            '  <td width="50%"> <span class="value">'+element.ruangan_name+'</span></td>'+
            '  <td width="33%"> <span class="value">'+element.ruangan_lokasi+'</span></td>'+
            '  <td width="15%" class="td-actions"><a href="ruangan#edit?id='+element.ruangan_ID+'" class="link-edit btn btn-small btn-utama"><i class="fa fa-pencil"></i> Edit</a> <a href="ruangan#hapus?id='+element.ruangan_ID+'" id="hapus_'+element.ruangan_ID+'" class="btn btn-secondary btn-small"><i class="fa fa-trash"></i> Hapus</a></td>'+
            '</tr>'
          );
        });

        /********************/
        /*  buat pagination */
        /********************/
        var pagination = '';
        var paging =  Math.ceil(data.total_rows / data.perpage) ;

        if( (!hal_aktif) && ($('ul#pagination-ruangan li').length == 0)){
          $('ul#pagination-ruangan li').remove();
          for(i = 1; i <= paging ; i++){
            pagination = pagination + '<li><a href="ruangan#ambil?hal='+i+'">'+i+'</a></li>';
          }
        }
        else if(hal_aktif && lokasi){
          $('ul#pagination-ruangan li').remove();
          for(i = 1; i <= paging ; i++){
            pagination = pagination + '<li><a href="ruangan#ambil?lokasi='+lokasi+'&hal='+i+'">'+i+'</a></li>';
          }
        }
        else if(hal_aktif && cari){
          $('ul#pagination-ruangan li').remove();
          for(i = 1; i <= paging ; i++){
            pagination = pagination + '<li><a href="ruangan#ambil?cari='+cari+'&hal='+i+'">'+i+'</a></li>';
          }
        }
        else if(hal_aktif){
          $('ul#pagination-ruangan li').remove();
          for (i = 1; i <= paging; i++) {
            pagination = pagination + '<li><a href="ruangan#ambil?&hal='+i+'">'+i+'</a></li>';
          }
        }

        $('ul#pagination-ruangan').append(pagination);
        $("ul#pagination-ruangan li:contains('"+hal_aktif+"')").addClass('active');

        /* UNTUK FILTER KATEGORI */
        $('#btn-filter-ruangan li').remove();
        $('#btn-filter-ruangan').append('<li><a href="ruangan">Semua Lokasi</a></li>');
        for(var i in data.all_location){
          $('#btn-filter-ruangan').append('<li><a href="ruangan#ambil?lokasi='+data.all_location[i]['ruangan_lokasi']+'&hal=1">'+data.all_location[i]['ruangan_lokasi']+'</a></li>');
        }

        if(scrolltop == true){$('body').scrollTop(0);}
      }

    });

  }
}

function ambil_user(hal_aktif, scrolltop, kategori, cari) {
  var path = window.location.pathname;
  var host = window.location.hostname;
  if ($('table#tbl-user').length > 0) {
    $.ajax({
      dataType : 'json',
      type : 'POST',
      url : 'http://'+host+path+'/action/ambil',
      data : {hal_aktif:hal_aktif, kategori:kategori, cari:cari},
      success : function (data) {

        $('table#tbl-user tbody tr').remove();
        $('table#tbl-user thead tr').remove();

        $('table#tbl-user').find('thead').append(
          '<tr>'+
          ' <th width="2%"></th>'+
          ' <th width="23%">Username</th>'+
          ' <th width="15%">Email</th>'+
          ' <th width="15%">Contact</th>'+
          ' <th width="15%">User Tipe</th>'+
          ' <th width="15%">User Role</th>'+
          ' <th width="15%"></th>'+
          '</tr>'
        );

        $.each(data.record, function(index, element) {

          $('table#tbl-user').find('tbody').append(
            '<tr>'+
            '  <td width="2%"><input type="checkbox" name="user_id[]" value="'+element.user_ID+'"></td>'+
            '  <td width="23%"> <span class="value">'+element.user_name+'</span></td>'+
            '  <td width="15%"> <span class="value">'+element.user_email+'</span></td>'+
            '  <td width="15%"> <span class="value">'+element.user_contact+'</span></td>'+
            '  <td width="15%"> <span class="value">'+element.user_type_name+'</span></td>'+
            '  <td width="15%"> <span class="value">'+element.user_role+'</span></td>'+
            '  <td width="15%" class="td-actions"><a href="user#edit?id='+element.user_ID+'" class="link-edit btn btn-small btn-utama"><i class="fa fa-pencil"></i> Edit</a> <a href="user#hapus?id='+element.user_ID+'" id="hapus_'+element.user_ID+'" class="btn btn-secondary btn-small"><i class="fa fa-trash"></i> Hapus</a></td>'+
            '</tr>'
          );
        });

        /********************/
        /*  buat pagination */
        /********************/
        var pagination = '';
        var paging =  Math.ceil(data.total_rows / data.perpage) ;

        if( (!hal_aktif) && ($('ul#pagination-user li').length == 0)){
          $('ul#pagination-user li').remove();
          for(i = 1; i <= paging ; i++){
            pagination = pagination + '<li><a href="user#ambil?hal='+i+'">'+i+'</a></li>';
          }
        }
        else if(hal_aktif && kategori){
          $('ul#pagination-user li').remove();
          for(i = 1; i <= paging ; i++){
            pagination = pagination + '<li><a href="user#ambil?kategori='+kategori+'&hal='+i+'">'+i+'</a></li>';
          }
        }
        else if(hal_aktif && cari){
          $('ul#pagination-user li').remove();
          for(i = 1; i <= paging ; i++){
            pagination = pagination + '<li><a href="user#ambil?cari='+cari+'&hal='+i+'">'+i+'</a></li>';
          }
        }
        else if(hal_aktif){
          $('ul#pagination-user li').remove();
          for (i = 1; i <= paging; i++) {
            pagination = pagination + '<li><a href="user#ambil?&hal='+i+'">'+i+'</a></li>';
          }
        }

        $('ul#pagination-user').append(pagination);
        $("ul#pagination-user li:contains('"+hal_aktif+"')").addClass('active');

        /* UNTUK FILTER KATEGORI */
        $('#btn-filter-user li').remove();
        $('#btn-filter-user').append('<li><a href="user">Semua Tipe</a></li>');
        for(var i in data.all_user_type){
          $('#btn-filter-user').append('<li><a href="user#ambil?kategori='+data.all_user_type[i]['user_type_name']+'&hal=1">'+data.all_user_type[i]['user_type_name']+'</a></li>');
        }

        if(scrolltop == true){$('body').scrollTop(0);}

      }
    });
  }
}

function ambil_user_type(hal_aktif, scrolltop, cari, user_type = 'user_type') {
  var path = window.location.pathname;
  var host = window.location.hostname;
  if ($('table#tbl-user-type').length > 0) {
    $.ajax({
      dataType : 'json',
      type : 'POST',
      url : 'http://'+host+path+'/ambil',
      data : {hal_aktif:hal_aktif, cari:cari, user_type:user_type},
      success : function (data) {

        $('table#tbl-user-type tbody tr').remove();
        $('table#tbl-user-type thead tr').remove();

        $('table#tbl-user-type').find('thead').append(
          '<tr>'+
          ' <th width="2%"></th>'+
          ' <th width="30%">Tipe</th>'+
          ' <th width="30%">Kategori</th>'+
          ' <th width="23%">Color</th>'+
          ' <th width="15%"></th>'+
          '</tr>'
        );

        $.each(data.record, function(index, element) {

          $('table#tbl-user-type').find('tbody').append(
            '<tr>'+
            '  <td width="2%"><input type="checkbox" name="user_type_id[]" value="'+element.user_type_ID+'"></td>'+
            '  <td width="30%"> <span class="value">'+element.user_type_name+'</span></td>'+
            '  <td width="30%"> <span class="value">'+element.user_type_category+'</span></td>'+
            '  <td width="23%"> <span class="value" style="background-color : #'+element.user_type_color+'; padding: 10px">'+element.user_type_color+'</span></td>'+
            '  <td width="15%" class="td-actions"><a href="type#edit?id='+element.user_type_ID+'" class="link-edit btn btn-small btn-utama"><i class="fa fa-pencil"></i> Edit</a> <a href="type#hapus?id='+element.user_type_ID+'" id="hapus_'+element.user_type_ID+'" class="btn btn-secondary btn-small"><i class="fa fa-trash"></i> Hapus</a></td>'+
            '</tr>'
          );
        });

        /********************/
        /*  buat pagination */
        /********************/
        var pagination = '';
        var paging =  Math.ceil(data.total_rows / data.perpage) ;

        if( (!hal_aktif) && ($('ul#pagination-user-type li').length == 0)){
          $('ul#pagination-user-type li').remove();
          for(i = 1; i <= paging ; i++){
            pagination = pagination + '<li><a href="type#ambil?hal='+i+'">'+i+'</a></li>';
          }
        }
        else if(hal_aktif && cari){
          $('ul#pagination-user-type li').remove();
          for(i = 1; i <= paging ; i++){
            pagination = pagination + '<li><a href="type#ambil?cari='+cari+'&hal='+i+'">'+i+'</a></li>';
          }
        }
        else if(hal_aktif){
          $('ul#pagination-user-type li').remove();
          for (i = 1; i <= paging; i++) {
            pagination = pagination + '<li><a href="type#ambil?&hal='+i+'">'+i+'</a></li>';
          }
        }

        $('ul#pagination-user-type').append(pagination);
        $("ul#pagination-user-type li:contains('"+hal_aktif+"')").addClass('active');

        if(scrolltop == true){$('body').scrollTop(0);}

      }
    });
  }
}

function ambil_peminjaman(lokasi, param) {
  var path = window.location.pathname;
  var host = window.location.hostname;

  var ruangan = getJSON('http://'+host+path+'/data/resources', {lokasi:lokasi});

  console.log(ruangan);

  if (ruangan != 'null') {
    $('#calendar').fullCalendar({
        now: moment.today,
        // editable: true,
        aspectRatio: 3.2,
        locale: moment.locale('id'),
        scrollTime: '00:00',
        header: {
          left: '',
          center: 'title',
          right : 'today, prev,next'
        },
        defaultView: 'timelineMonth',
        resourceAreaWidth: '15%',
        resourceLabelText: 'Ruangan',
        resources: function(callback){
          if(param)  {
            rooms=ruangan;
            console.log(rooms);
          }
          else{
            rooms=ruangan;
            console.log(ruangan);
          }
          callback(rooms);
        },
        eventSources: [{
          url: 'http://'+host+path+'/data/events',
          dataType: 'json',
          data: {lokasi:lokasi},
          success: function(doc) {
            console.log(doc);
          },
        }],
        dayClick: function(date, jsEvent, view, resources ) {
          var htmlStr = "";

          htmlStr = htmlStr + '<option value=""> -- Pilih user -- </option>';

          for (var i = 0; i < resources.user.length; i++) {
            htmlStr = htmlStr + '<option value="'+resources.user[i].user_ID+'"> '+ resources.user[i].user_name +' </option>';
            $('#form-peminjaman #peminjaman_user').html(htmlStr);
          }

          $('#myModal .modal-header #myModalLabel').text('Tambah Peminjaman');
          $('#myModal .modal-body #peminjaman_start').val(date.format());
          $('#myModal .modal-body #peminjaman_end').val(date.format());
          $('#myModal .modal-body #peminjaman_ruangan').val(resources.id);
          $('#myModal .modal-footer #submit-peminjaman').text('Tambah!');
          $('#myModal #form-peminjaman').attr('action', 'tambah');
          $('#myModal .modal-footer #hapus-peminjaman').remove();
          $('#myModal').modal('show');

        },
        eventClick: function(calEvent, jsEvent, view) {
          var peminjaman_ID = calEvent.id;
          var user = calEvent.user.data;
          var peminjaman_detail = getJSON('http://'+host+path+'/data/events', {peminjaman_ID:peminjaman_ID});
          var htmlStr = "";

          $('#form-peminjaman #peminjaman_user option').remove();
          // $('#form-peminjaman #peminjaman_user').append('<option value="">-- Pilih User --</option>');
          for(var i in user){
            $('#form-peminjaman #peminjaman_user').append('<option value="'+user[i].user_ID+'">'+user[i].user_name+'</option>');
          }

          if (calEvent.user.user_ID) {
            $('#form-peminjaman #peminjaman_user option[value='+calEvent.user.user_ID+']').prop('selected', true);
          }

          $('#myModal .modal-body #peminjaman_title').val(peminjaman_detail.data['peminjaman_title']);
          $('#myModal .modal-body #peminjaman_start').val(calEvent.start.format());
          $('#myModal .modal-body #peminjaman_end').val(calEvent.start.format());
          $('#myModal .modal-body #peminjaman_ruangan').val(calEvent.resourceId);
          $('#myModal .modal-header #myModalLabel').text('Detail Peminjaman');
          $('#myModal .modal-footer #submit-peminjaman').text('Update!');
          $('#myModal #form-peminjaman').attr('action', 'update');
          $('#myModal #form-peminjaman #peminjaman_id').val(peminjaman_ID);
          $('#myModal .modal-footer').append('<button class="btn btn-danger" id="hapus-peminjaman">hapus!</button>');
          $('#myModal').modal('show');
        }
    });
  }

}

function ambil_pengumuman(hal_aktif, scrolltop, cari) {
  var path = window.location.pathname;
  var host = window.location.hostname;
  if ($('table#tbl-pengumuman').length > 0) {
    $.ajax({
      dataType : 'json',
      type : 'POST',
      url : 'http://'+host+path+'/action/ambil',
      data : {hal_aktif:hal_aktif, cari:cari},
      success : function (data) {

        $('table#tbl-pengumuman tbody tr').remove();
        $('table#tbl-pengumuman thead tr').remove();

        $('table#tbl-pengumuman').find('thead').append(
          '<tr>'+
          ' <th width="2%"></th>'+
          ' <th width="15%">Judul</th>'+
          ' <th width="15%">Kepada</th>'+
          ' <th width="15%">Dari</th>'+
          ' <th width="15%">Tanggal</th>'+
          ' <th width="25%">Konten</th>'+
          ' <th width="13%"></th>'+
          '</tr>'
        );

        $.each(data.record, function(index, element) {

          $('table#tbl-pengumuman').find('tbody').append(
            '<tr>'+
            '  <td width="2%"><input type="checkbox" name="pengumuman_id[]" value="'+element.pengumuman_ID+'"></td>'+
            '  <td width="15%"> <span class="value">'+element.pengumuman_title+'</span></td>'+
            '  <td width="15%"> <span class="value">'+element.pengumuman_to+'</span></td>'+
            '  <td width="15%"> <span class="value">'+element.pengumuman_from+'</span></td>'+
            '  <td width="15%"> <span class="value">'+element.pengumuman_date+'</span></td>'+
            '  <td width="25%"> <span class="value">'+element.pengumuman_content+'</span></td>'+
            '  <td width="13%" class="td-actions"><a href="pengumuman#edit?id='+element.pengumuman_ID+'" class="link-edit btn btn-small btn-utama"><i class="fa fa-pencil"></i> Edit</a> <a href="pengumuman#hapus?id='+element.pengumuman_ID+'" id="hapus_'+element.user_type_ID+'" class="btn btn-secondary btn-small"><i class="fa fa-trash"></i> Hapus</a></td>'+
            '</tr>'
          );
        });

      }
    });
  }
}

function humanize(str) {
  str = str.replace(/-/g, ' ');
  str = str.replace(/_/g, ' ');
  return str.charAt(0).toUpperCase() + str.slice(1);
}

function getJSON(url,data){
  return JSON.parse($.ajax({
    type: 'POST',
    url : url,
    data: data,
    dataType:'json',
    global: false,
    async: false,
    success:function(msg){

    }
  }).responseText);
}

function getUrlVars() {
  var vars = [], hash;
  var hashes = window.location.href.slice(window.location.href.indexOf('?')+1).split('&');
  for (var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split('=');
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }
  return vars;
}
